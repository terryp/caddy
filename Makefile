SHELL = /bin/sh
.DEFAULT_GOAL := build

python=python
poetry=${python} -m poetry
pip=${python} -m pip ${pip_flags}

pytest=${poetry} run pytest
flake8=${poetry} run flake8
isort=${poetry} run isort .

src_files=${wildcard omniblack/caddy/**/*.py}

install_cmd=${pip} install
uninstall_cmd=${pip} uninstall

install-deps: poetry.lock pyproject.toml .venv
	@echo "Installing depencies"
	${poetry} install

dist: ${src_files} pyproject.toml
	@echo "Building wheels"
	rm ./dist -rf
	${poetry} build

publish: dist
	@echo "Uploading files to pypi"
	${poetry} publish

test:
	@echo "Running tests"
	${pytest}

install: dist
	@echo "Installing"
	${install_cmd} ./dist/*.whl

uninstall:
	@echo "Uninstalling"
	${uninstall_cmd} omniblack.caddy

clean:
	@echo "Cleaning built wheels"
	@echo "run make dist to rebuild the wheels"
	rm ./dist -rf

lint:
	${flake8}
	${isort}


.PHONY: test;


